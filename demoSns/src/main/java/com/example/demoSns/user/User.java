package com.example.demoSns.user;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name="users")
public class User {
	
	@Id
	@Column(name="userno")
	private String no;

	@Column(name="userid")
	private String id;

	@Column(name="username")
	private String name;
}
